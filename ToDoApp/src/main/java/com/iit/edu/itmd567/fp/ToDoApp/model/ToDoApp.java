/**
 * 
 */
package com.iit.edu.itmd567.fp.ToDoApp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Admin
 *
 */
@Entity
@Table(name = "ToDoApp_info")
public class ToDoApp {

	private String taskID;
	private String taskName;
	private Date startDate;
	private Date endDate;
	private String isActive;

	public ToDoApp() {
		super();
	}

	public ToDoApp(String taskName, Date startDate, Date endDate, String isActive) {
		super();
		this.taskName = taskName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.isActive = isActive;
	}

	public String getTaskID() {
		return taskID;
	}

	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "ToDoApp [taskID=" + taskID + ", taskName=" + taskName + ", startDate=" + startDate + ", endDate="
				+ endDate + ", isActive=" + isActive + "]";
	}

}
