/**
 * 
 */
package com.iit.edu.itmd567.fp.ToDoApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iit.edu.itmd567.fp.ToDoApp.model.ToDoApp;

/**
 * @author Admin
 *
 */
public interface ToDoAppRepository extends JpaRepository<ToDoApp, String> {

}
